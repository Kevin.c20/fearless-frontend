function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitule mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer" <time>${startDate} - ${endDate}</time></div>
        </div>
      </div>
    `;
  }
  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col')
    let colIndx = 0
    try{
    const response = await fetch(url);
    if (!response.ok){
        throw new Error('Response not ok');
    } else {
        const data = await response.json();
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startDate = new Date(details.conference.starts).toLocaleDateString();
              const endDate = new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, startDate, endDate, location);
              const column = columns[colIndx % 3];
              column.innerHTML += html;
              colIndx = (colIndx + 1) % 3
        }
    }
   }
  } catch (e) {
    console.error('error', e)
  }
  });
// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         throw new Error("Hello")

//       } 
//       else {
//         const data = await response.json();
//         const conference = data.conferences[1];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;
  
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const description = document.querySelector('.card-text');
//             description.innerHTML = details.conference.description;
//             const imgTag = document.querySelector(".card-img-top");
//             imgTag.src = details.conference.location.picture_url;
//         }
  
//       }
//     } catch (e) {
//       console.error("NO")
//     }
  
//   });
  
  